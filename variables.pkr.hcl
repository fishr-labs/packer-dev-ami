variable "ami_name" {
  type = string
}

variable "instance_type" {
  type = string
}

variable "region" {
  type = string
}

variable "source_ami" {
  type = string
}

variable "ssh_username" {
  type = string
}

variable "delete_on_termination" {
  type = bool
  default = true
}

variable "volume_type" {
  type = string
  default = "gp2"
}

variable "volume_size" {
  type = number
}

variable "volume_device_name" {
  type = string
}

