packer {
  required_plugins { 
    amazon = {
      version = ">= 1.2.8"
      source  = "github.com/hashicorp/amazon"
    }
    ansible = {
      version = "~> 1"
      source = "github.com/hashicorp/ansible"
    }
  }
}

locals {
  timestamp = regex_replace(timestamp(), "[- TZ:]", "")
}

source "amazon-ebs" "revolve_dev" {

  ami_name      = var.ami_name
  instance_type = var.instance_type
  region        = var.region
  source_ami = var.source_ami
  ssh_username = var.ssh_username

  launch_block_device_mappings {
    device_name = var.volume_device_name
    volume_size = var.volume_size
    volume_type = var.volume_type
    delete_on_termination = var.delete_on_termination
  }

  tags = { 
    Name = var.ami_name
  }
}

build {
  name    = "revolve_dev_builder"
  sources = [
    "source.amazon-ebs.revolve_dev"
  ]

  provisioner "ansible" {
    playbook_file = "./ansible/main.yml"
    use_proxy = false
  }

}
