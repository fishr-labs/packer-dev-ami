# Packer Development AMI

## Instructions

### 1. Update the ansible inventory variables

```bash
mv ./ansible/inventory.example.yml ./ansible/inventory.yml
```

```yml
# ansible/inventory.yml
all:
  hosts:
    wintermute:
      ansible_host: <your_ip_address>
      ansible_user: <default_username>
```

### 2. Update the global variables

```bash
mv ./ansible/group_vars/all.example.yml ./ansible/group_vars/all.yml
```

```yml
# ansible/group_vars/all.yml
new_user: <name_of_your_new_user>
pubkey_path: "path/to/your/ssh/public/key"
default_roles:
  # Feel free to add / remove roles to fit your needs
  - system
  - hashicorp
  - docker
  - go
  - kubernetes
  - users
  - user_setup
  - rust
  - python
  - node
  - neovim
  - dotfiles
```

### 3. Run the playbook

```bash
ansible-playbook -i ./ansible/inventory.yml ./ansible/main.yml --private-key /path/to/your/private-key
```

and ... voila !

### 4. (Optional) Build an AMI for EC2

#### 1. Create a variables file with the following content

```bash
touch variables.pkrvars.hcl
```

```hcl
ami_name="my-cool-ami"
instance_type="t3.micro"
region="your-aws-region-code"
source_ami="ami-id-to-be-used-to-create-your-image"
ssh_username="default-username-for-the-ami"
```

Note on the `ssh_username` variable: the value should correspond to the default username created on your `source_ami`.

For instance, if you choose to base your image off of a Debian AMI, the default username will be `admin`, for Amazon Linux it's `ec2-user` etc ...

#### 2. Build your image

```bash
packer build -var-file=variables.pkvars.hcl .
```

And boom ! You should have a newly created AMI in your region that you can use to create instances from !
